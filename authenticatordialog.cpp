#include "authenticatordialog.h"
#include "ui_authenticatordialog.h"

AuthenticatorDialog::AuthenticatorDialog(QWidget *parent, const QString& realm) :
    QDialog(parent),
    ui(new Ui::AuthenticatorDialog)
{
    ui->setupUi(this);
    ui->label_Message->hide();
    QString txt = ui->label_3->text();
    ui->label_3->setText(txt.arg(realm));
    ui->lineEditUser->setFocus();
}

AuthenticatorDialog::~AuthenticatorDialog()
{
    delete ui;
}

QString AuthenticatorDialog::passwd() const
{
    return ui->lineEditPasswd->text();
}

QString AuthenticatorDialog::user() const
{
    return ui->lineEditUser->text();
}

QString AuthenticatorDialog::message() const
{
    return ui->label_Message->text();
}

void AuthenticatorDialog::setPasswd(QString arg)
{
    if (m_passwd == arg)
        return;
    ui->lineEditPasswd->setText(arg);
    m_passwd = arg;
    emit passwdChanged(arg);
}

void AuthenticatorDialog::setUser(QString arg)
{
    if (m_user == arg)
        return;
    ui->lineEditUser->setText(arg);
    m_user = arg;
    emit userChanged(arg);
}

void AuthenticatorDialog::setMessage(QString arg)
{
    ui->label_Message->setText(arg);
    ui->label_Message->show();
}
