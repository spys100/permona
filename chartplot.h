#ifndef CHARTPLOT_H
#define CHARTPLOT_H

#include <QWidget>
#include <QChartView>

typedef QVector<double> DoubleVector;

namespace QtCharts {
class QDateTimeAxis;
}

class ChartPlot : public QtCharts::QChartView
{
    Q_OBJECT
public:
    explicit ChartPlot(const QString& plotid, QWidget *parent = 0);
    void addSeries(const QString& name, const DoubleVector &x, const DoubleVector &y);

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
private:
    QtCharts::QDateTimeAxis *m_axisX;
signals:
    void checked(const QVariant& itemInfo, bool on, int index);
    void addDataSet(const QString& plotid, const QString& dest);
public slots:
protected slots:
    void handleMarkerClicked();
};

#endif // CHARTPLOT_H
