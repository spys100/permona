#ifndef NAGIOSADAPTER_H
#define NAGIOSADAPTER_H

#include <QObject>
#include <QDateTime>
#include <QSslError>

class QJsonDocument;
class QNetworkAccessManager;
class QNetworkReply;
class QAuthenticator;

/// a list of counters.
typedef QStringList PlotIdList;
typedef QVector<double> DoubleVector;

extern const QString PLOTID_SEP;

class NagiosAdapter : public QObject
{
    Q_OBJECT
public:
    explicit NagiosAdapter(QObject *parent = 0);
signals:
    //void finished(const QJsonDocument& doc);
    /**
     * @brief emmitted when hosts and services are received from nagios/icinga
     * @param hsmp A map with host as key. Value contains associated services.
     */
    void receivedPlotIds(const PlotIdList& hsmp);
    /**
     * @brief emitted when perfdata arrives from nagios/icinga
     * @param plotid The id of the plot the data was requested for
     * @param x time values
     * @param y corresponding sample values
     */
    void receivedPerfData(const QString& plotid, const QString& dest, const DoubleVector& x, const DoubleVector& y );
    /**
     * @brief emmitted when something goes wrong
     * @param errStr a string saying what has failed
     */
    void error(const QString& errStr);
public slots:
    /**
     * @brief getPerfData
     * @param plotid
     * @param dest
     * @param start Start time of dataset
     * @param end End time of dataset
     */
    void getPerfData(const QString &plotid, const QString &dest, const QDateTime start=QDateTime(), const QDateTime end=QDateTime());
    void getPlotIds();
protected slots:
    /**
     * @brief replyFinished
     * @param reply
     */
    void replyFinished(QNetworkReply *reply);
    void authenticate(QNetworkReply*reply, QAuthenticator*auth);
    void onSslErrors(QNetworkReply *reply, QList<QSslError> errors);
private:
    void parseHSMap(const QJsonDocument& doc, PlotIdList& plotids);
    /**
     * @brief convert the json perf data to x (time) and corresponding y values
     * @param doc json perf data
     * @param x time axis
     * @param y values corresponding to time
     */
    void parsePerfData(const QJsonDocument& doc, const QString &plotid, DoubleVector& x, DoubleVector &y);

    QUrl url() const;

    QJsonDocument *m_doc;
    QNetworkAccessManager *m_networkmanager;
    void getCounterNames(const QStringList &hsm);
};

#endif // NAGIOSADAPTER_H
