#ifndef AUTHENTICATORDIALOG_H
#define AUTHENTICATORDIALOG_H

#include <QDialog>

namespace Ui {
class AuthenticatorDialog;
}

class AuthenticatorDialog : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(QString passwd READ passwd WRITE setPasswd NOTIFY passwdChanged)
    Q_PROPERTY(QString user READ user WRITE setUser NOTIFY userChanged)
    Q_PROPERTY(QString message READ message WRITE setMessage)
public:
    explicit AuthenticatorDialog(QWidget *parent = 0, const QString &realm = QString());
    ~AuthenticatorDialog();
    QString passwd() const;
    QString user() const;

    QString message() const;

public slots:
    void setPasswd(QString arg);

    void setUser(QString arg);

    void setMessage(QString arg);

signals:
    void passwdChanged(QString arg);

    void userChanged(QString arg);

private:
    Ui::AuthenticatorDialog *ui;
    QString m_passwd;
    QString m_user;
};

#endif // AUTHENTICATORDIALOG_H
