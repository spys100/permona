#include "chartplot.h"
#include <QtCharts>

using namespace QtCharts;

ChartPlot::ChartPlot(const QString &plotid, QWidget *parent)
    : QChartView(parent)
    , m_axisX(new QDateTimeAxis())
{
    setAcceptDrops(true);
    setAttribute(Qt::WA_DeleteOnClose);
    setChart(new QChart);

    setRenderHint(QPainter::Antialiasing);

    QPushButton *cb = new QPushButton(QIcon(":/images/close.png"),"", this);
    cb->setFlat(true);
    cb->setGeometry(0, 0, 20, 20);
    connect(cb, &QPushButton::clicked, this, &QtCharts::QChartView::close);

    setObjectName(plotid);
    setProperty("plotid", plotid);
    chart()->setTitle(plotid);

    m_axisX->setTickCount(10);
    m_axisX->setFormat("HH:mm:ss");
    m_axisX->setTitleText("Date");
    chart()->addAxis(m_axisX, Qt::AlignBottom);

    //w->setProperty("service",svc);
}

void ChartPlot::addSeries(const QString& name, const DoubleVector &x, const DoubleVector &y)
{
    QLineSeries *series = new QLineSeries();
    series->setName(name);
    for (int i=0; i< x.length(); i++ ){
        series->append(x[i] , y[i]);
    }
    chart()->addSeries(series);

    // Connect all markers to handler
    foreach (QLegendMarker* marker, chart()->legend()->markers()) {
        // Disconnect possible existing connection to avoid multiple connections
        QObject::disconnect(marker, SIGNAL(clicked()), this, SLOT(handleMarkerClicked()));
        QObject::connect(marker, SIGNAL(clicked()), this, SLOT(handleMarkerClicked()));
    }

    QValueAxis *axisY = new QValueAxis;
    axisY->setLabelFormat("%i");
    axisY->setTitleText("Value");
    chart()->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);
    series->attachAxis(m_axisX);
}

void ChartPlot::handleMarkerClicked()
{
    QLegendMarker* marker = qobject_cast<QLegendMarker*> (sender());
    Q_ASSERT(marker);

    switch (marker->type()){

    case QLegendMarker::LegendMarkerTypeXY:{
        // Toggle visibility of series
        marker->series()->setVisible(!marker->series()->isVisible());

        // Turn legend marker back to visible, since hiding series also hides the marker
        // and we don't want it to happen now.
        marker->setVisible(true);

        // Dim the marker, if series is not visible
        qreal alpha = 1.0;

        if (!marker->series()->isVisible()) {
            alpha = 0.5;
        }

        QColor color;
        QBrush brush = marker->labelBrush();
        color = brush.color();
        color.setAlphaF(alpha);
        brush.setColor(color);
        marker->setLabelBrush(brush);

        brush = marker->brush();
        color = brush.color();
        color.setAlphaF(alpha);
        brush.setColor(color);
        marker->setBrush(brush);

        QPen pen = marker->pen();
        color = pen.color();
        color.setAlphaF(alpha);
        pen.setColor(color);
        marker->setPen(pen);

        break;
    }
    default:{
        qDebug() << "Unknown marker type";
        break;
    }
    }
}

void ChartPlot::dragEnterEvent(QDragEnterEvent *event)
{
    qDebug() << "dragEnterEvent" << event->mimeData()->formats();
    if (event->mimeData()->hasFormat("application/vnd.plot.list"))
        event->acceptProposedAction();
}

void ChartPlot::dropEvent(QDropEvent *event)
{
    QByteArray encodedData = event->mimeData()->data("application/vnd.plot.list");
    QDataStream stream(&encodedData, QIODevice::ReadOnly);

    QString plotid;
    stream >> plotid;

    event->acceptProposedAction();

    emit addDataSet(plotid, objectName());
}
