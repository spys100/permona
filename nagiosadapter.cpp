#include "nagiosadapter.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QAuthenticator>
#include <QApplication>
#include <QLoggingCategory>
#include <QSettings>
#include <QUrlQuery>
#include "mainwindow.h"

Q_LOGGING_CATEGORY(NagiosConnection,"nagios.connection")

#define QNETWORKREQ_PLOTID QNetworkRequest::Attribute(int(QNetworkRequest::User) + 1)
#define QNETWORKREQ_DEST QNetworkRequest::Attribute(int(QNetworkRequest::User) + 2)

NagiosAdapter::NagiosAdapter(QObject *parent) :
    QObject(parent)
{
    m_networkmanager = new QNetworkAccessManager(this);
    connect(m_networkmanager, SIGNAL(finished(QNetworkReply*)), SLOT(replyFinished(QNetworkReply*)));
    connect(m_networkmanager, &QNetworkAccessManager::authenticationRequired, this, &NagiosAdapter::authenticate);
    connect(m_networkmanager, SIGNAL(sslErrors(QNetworkReply*,QList<QSslError>)), SLOT(onSslErrors(QNetworkReply*,QList<QSslError>)));
}

void NagiosAdapter::onSslErrors(QNetworkReply *reply, QList<QSslError> errors)
{
    QSslError error;
    foreach (error, errors) {
        qCDebug(NagiosConnection) << error.certificate().subjectInfo(QSslCertificate::CommonName)
                 << "," << error.certificate().subjectAlternativeNames().values()
                 << ":" << error;
        if (error.error() == QSslError::HostNameMismatch ||
            error.error() == QSslError::UnableToGetLocalIssuerCertificate)
            reply->ignoreSslErrors();
    }
}

void NagiosAdapter::replyFinished(QNetworkReply* reply)
{
    QJsonParseError err;
    QJsonDocument doc;
    QByteArray ba;
    DoubleVector x, y;
    QString plotid = reply->request().attribute(QNETWORKREQ_PLOTID).toString();
    QString dest = reply->request().attribute(QNETWORKREQ_DEST).toString();

    //check for errors
    if (reply->error() != QNetworkReply::NoError) {
        emit error(reply->errorString());
    }
    ba = reply->readAll();
    doc = QJsonDocument::fromJson(ba,&err);

    if (reply->request().attribute(QNetworkRequest::User) == "perfData"){
        parsePerfData(doc, plotid, x, y);
        emit receivedPerfData(plotid, dest, x, y);
    }else if (reply->request().attribute(QNetworkRequest::User) == "counterNames"){
        QJsonArray legend = doc.object()["meta"].toObject()["legend"].toObject()["entry"].toArray();
        QStringList curveTitles;
        //create a curve with the name of curve from legend
        foreach (QJsonValue entry, legend) {
            QString name=entry.toString("_untitled_");
            if ( name.endsWith("_AVERAGE"))
                curveTitles.append(plotid + PLOTID_SEP + name.split('_').first());
        }
        emit receivedPlotIds(curveTitles);
    }else if (reply->request().attribute(QNetworkRequest::User) == "servicemap"){
        PlotIdList hsm;
        parseHSMap(doc,hsm);
        getCounterNames(hsm);
    }
    reply->deleteLater();
}

void NagiosAdapter::authenticate(QNetworkReply *reply, QAuthenticator *auth)
{
    Q_UNUSED(reply);
    QString user=auth->user();
    QString passwd=auth->password();
    MainWindow *mainwnd = qobject_cast<MainWindow*>(parent());
    Q_ASSERT(mainwnd);
    mainwnd->authenticate(auth->realm(), &user, &passwd);
    auth->setUser(user);
    auth->setPassword(passwd);
}

void NagiosAdapter::parseHSMap(const QJsonDocument &doc, PlotIdList &plotids)
{
    QJsonValue entry;
    QJsonArray a = doc.object()["status"].toObject()["service_status"].toArray();
    foreach (entry, a) {
        QString host = entry.toObject()["host_name"].toString();
        QString service = entry.toObject()["service_description"].toString();
        if (entry.toObject()["action_url"].toString().contains("pnp4nagios"))
            plotids.append(host + PLOTID_SEP + service);
    }
}

void NagiosAdapter::parsePerfData(const QJsonDocument &doc, const QString&plotid, DoubleVector &x, DoubleVector &y)
{
    x.clear();
    y.clear();
    int col = 0;
    QJsonArray row = doc.object()["data"].toObject()["row"].toArray();
    QJsonArray legend = doc.object()["meta"].toObject()["legend"].toObject()["entry"].toArray();
    //find the offset for selected plotid
    QString name = plotid.split(PLOTID_SEP).last() + "_AVERAGE";
    foreach (QJsonValue entry, legend) {
        if (entry.toString() == name) break;
        col++;
    }

    foreach (QJsonValue entry, row) {
        QJsonArray values = entry.toObject()["v"].toArray();

        QString nas = values[col].toString();
        if (nas == "NaN") continue;

        QDateTime dt;
        dt.setTime_t(entry.toObject()["t"].toString().toUInt());
        x.append(dt.toMSecsSinceEpoch());
        y.append(nas.toDouble());
    }
}

QUrl NagiosAdapter::url() const
{
    QSettings settings;
    return QUrl(settings.value(QStringLiteral("NAGIOS_URL")).toString());
}

static inline QString d2pnpTime(const QDateTime t)
{
    return t.toString("yyyyMMddhh:mm:ss");
}

void NagiosAdapter::getCounterNames(const QStringList& hsm)
{
#ifdef SIMULATION
    auto requestCounterName = [this](QString hostService){
        QUrl url("file://"+ qgetenv("HOME") + "/nagios_perf.json");
        QStringList hs = hostService.split(PLOTID_SEP);
        QNetworkRequest req(url);
        req.setAttribute(QNetworkRequest::User,"counterNames");
        req.setAttribute(QNETWORKREQ_PLOTID, hostService);
        //req.setAttribute(QNETWORKREQ_DEST, "");
        m_networkmanager->get(req);
    };

    foreach (QString hs, hsm) {
        requestCounterName(hs);
    }
#else

    auto requestCounterName = [this](QString hostService){
        QUrl url = this->url();
        url.setPath("/pnp4nagios/xport/json");// ?host=%2&srv=%3
        // time ranges format YYYYMMDDhh:mm:ss i.e. 2011102322:50:00 specifies "23.10.2011 22:50:00"
        QUrlQuery query;
        QStringList hs = hostService.split(PLOTID_SEP);
        query.addQueryItem("host", hs.value(0));
        query.addQueryItem("srv", QString(hs.value(1)).replace(' ','_'));
        QString currentTime = d2pnpTime(QDateTime::currentDateTime());
        query.addQueryItem("start", currentTime);
        query.addQueryItem("end", currentTime);

        url.setQuery(query);
        QNetworkRequest req(url);
        req.setAttribute(QNetworkRequest::User,"counterNames");
        req.setAttribute(QNETWORKREQ_PLOTID, hostService);
        //req.setAttribute(QNETWORKREQ_DEST, "");
        m_networkmanager->get(req);
    };


    foreach (QString hs, hsm) {
        requestCounterName(hs);
    }
#endif
}

void NagiosAdapter::getPerfData(const QString& plotid, const QString& dest, const QDateTime start, const QDateTime end)
{
    DoubleVector x, y;
#ifdef SIMULATION
    QUrl url("file://"+ qgetenv("HOME") + "/nagios_perf.json");
    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::User,"perfData");
    req.setAttribute(QNETWORKREQ_PLOTID, plotid);
    req.setAttribute(QNETWORKREQ_DEST, dest);
    m_networkmanager->get(req);
#else
    QUrl url = this->url();
    url.setPath("/pnp4nagios/xport/json");// ?host=%2&srv=%3
    //static const QString JSON_DATA_REQ(ICINGA_SCHEME + "://%1");
    // time ranges format YYYYMMDDhh:mm:ss i.e. 2011102322:50:00 specifies "23.10.2011 22:50:00"
    QUrlQuery query;
    QStringList hs = plotid.split(PLOTID_SEP);
    query.addQueryItem("host", hs.value(0));
    query.addQueryItem("srv", QString(hs.value(1)).replace(' ','_'));

    if (!start.isNull()){  
        query.addQueryItem("start", d2pnpTime(start));
    }
    if (!end.isNull()){
        query.addQueryItem("end", d2pnpTime(end));
    }
    url.setQuery(query);
    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::User,"perfData");
    req.setAttribute(QNETWORKREQ_PLOTID, plotid);
    req.setAttribute(QNETWORKREQ_DEST, dest);
    m_networkmanager->get(req);
#endif
}

void NagiosAdapter::getPlotIds()
{
#ifdef SIMULATION
    QNetworkRequest req;
    QUrl url("file://" + qgetenv("HOME") + "/Downloads/status-1.cgi.json");
    req.setUrl(url);
    req.setAttribute(QNetworkRequest::User,"servicemap");
    m_networkmanager->get(req);
    emit error("----Simulation----");
#else
    //static const QString JSON_DATA_REQ( ICINGA_SCHEME + "://%1/icinga/cgi-bin/status.cgi?host=all&jsonoutput");
    QUrl url = this->url();
    url.setPath("/icinga/cgi-bin/status.cgi");
    QUrlQuery query;
    query.addQueryItem("host", "all");
    query.addQueryItem("jsonoutput","");
    url.setQuery(query);
    QNetworkRequest req;
    req.setUrl(url);
    req.setAttribute(QNetworkRequest::User,"servicemap");
    m_networkmanager->get(req);    
#endif
}
