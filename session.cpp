#include "session.h"
#include <QList>
#include <QSettings>
#include <QDebug>
#include "mainwindow.h"
#include "chartplot.h"

Session::Session(QObject *parent) : QObject(parent)
{

}

Session::~Session()
{
    save();
}

void Session::save()
{
    Q_ASSERT(parent());
    // iterate over all plot windows
    QList<ChartPlot *> plots = parent()->findChildren<ChartPlot *>();
    qDebug() << plots;
    QSettings settings;
    settings.beginGroup("SESSION");
    settings.beginWriteArray("plots");
    for (int i=0; i < plots.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue("plotid", plots.at(i)->objectName());
    }
    settings.endArray();
    settings.endGroup();
}

void Session::restore()
{
    QSettings settings;
    settings.beginGroup("SESSION");
    int size = settings.beginReadArray("plots");
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        QString plotid = settings.value("plotid").toString();
        qobject_cast<MainWindow*>(parent())->createPlot(plotid);
        qDebug() << "restore: " << plotid;
    }
    settings.endArray();
    settings.endGroup();
}

