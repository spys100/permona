#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QJsonDocument>
#include <QtCore>
#include <QFile>
#include <QPushButton>
#include <qlayout.h>
//#include <qwt_plot_curve.h>
//#include <qwt_plot_legenditem.h>
//#include <qwt_symbol.h>
#include <QChartView>
#include <QLineSeries>
#include "nagiosadapter.h"
#include "hsmmodel.h"
#include "authenticatordialog.h"
#include "chartplot.h"
#include "preferences.h"
#include "simplecrypt.h"
#include "about.h"
#include "session.h"


const QString PLOTID_SEP(':');

static const char *colors[] =
    {
        "LightSalmon",
        "SteelBlue",
        "Yellow",
        "Fuchsia",
        "PaleGreen",
        "PaleTurquoise",
        "Thistle",
        "HotPink",
        "Peru",
        "Maroon"
    };

static const int numColors = sizeof( colors ) / sizeof( colors[0] );

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_dataSource(new NagiosAdapter(this)),
    m_crypto(new SimpleCrypt(0xBE158743722E97E0))
{
    QCoreApplication::setOrganizationName("ximidi");
    QCoreApplication::setOrganizationDomain("ximidi.com");
    QCoreApplication::setApplicationName("Permona");
    Session *session = new Session(this);
    ui->setupUi(this);
    QSettings settings;
    // started for the first time?
    if (!settings.contains("NAGIOS_URL")){
        preferencesDlg();
    }

    ui->actionPreferences->setMenuRole(QAction::PreferencesRole);
    ui->actionAbout->setMenuRole(QAction::AboutRole);

    connect(ui->actionPreferences, &QAction::triggered, this, &MainWindow::preferencesDlg);
    connect(m_dataSource, &NagiosAdapter::receivedPlotIds, this, &MainWindow::updateServiceMap);
    connect(m_dataSource, &NagiosAdapter::receivedPerfData, this, &MainWindow::plotGraph);
    connect(ui->treeView, &QTreeView::doubleClicked, this, &MainWindow::on_itemSelected);
    connect(m_dataSource, SIGNAL(error(QString)), statusBar(), SLOT(showMessage(QString)));
    statusBar()->showMessage("initializing...", 10000);

    ui->treeView->setDragEnabled(true);
    ui->mainToolBar->hide();
    //request Host and Services
    QTimer::singleShot(1, m_dataSource, &NagiosAdapter::getPlotIds);
    QTimer::singleShot(1, session, &Session::restore);
}

void MainWindow::plotSingleGraph(ChartPlot *chart,const QString &title, const DoubleVector &x, const DoubleVector &y, const QColor &color)
{
    chart->addSeries(title, x, y);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateServiceMap(const PlotIdList &hsmp)
{
    if (ui->treeView->model()){
        //ui->treeView->model()->deleteLater();
        HsmProxyModel *proxy = qobject_cast<HsmProxyModel*>(ui->treeView->model());
        proxy->appendToRoot(hsmp);
    }else{
        QSortFilterProxyModel *proxyModel = new HsmProxyModel(this);
        proxyModel->setSourceModel(new HsmModel(hsmp, proxyModel));
        ui->treeView->setModel(proxyModel);
    }
}

void MainWindow::plotGraph(const QString &plotid, const QString &dest, const DoubleVector &x, const DoubleVector &y)
{
    QString title = plotid.split(PLOTID_SEP).last();
    ChartPlot *plot = findChild<ChartPlot*>(dest);
    if (!plot){
        qWarning() << "Could not find plot widget" << dest;
        return;
    }
    int i = 0;
    plotSingleGraph(plot, title, x, y, QColor( colors[ i++ % numColors ] ));
}

void MainWindow::authenticate(const QString &realm, QString *user, QString *passwd)
{
    const QString c_user=QStringLiteral("user");
    const QString c_password=QStringLiteral("password");

    QSettings settings;
    settings.beginGroup(QStringLiteral("credentials"));
    settings.beginGroup(realm);

    AuthenticatorDialog *dlg = new AuthenticatorDialog(this, realm);


    dlg->setPasswd(*passwd);
    dlg->setUser(*user);
    if (user->isEmpty() && settings.value(c_user).isValid()){
        *user = m_crypto->decryptToString(settings.value(c_user).toString());
        *passwd = m_crypto->decryptToString(settings.value(c_password).toString());
    }else{
        dlg->setMessage(tr("Invalid user or password."));
        if ( dlg->exec() == QDialog::Accepted ){
            *user = dlg->user();
            *passwd = dlg->passwd();
            settings.setValue(c_user, m_crypto->encryptToString(*user));
            settings.setValue(c_password, m_crypto->encryptToString(*passwd));
        } else {
            *user = QString();
            *passwd = QString();
            close();
        }
    }
    qDebug() << "authenticate as " << *user << "for" << realm;
}

void MainWindow::setTimeRange(const QDateTime start, const QDateTime end)
{
    //for all plot windows
    QList<ChartPlot*> plots = findChildren<ChartPlot*>();
    foreach (ChartPlot *plot, plots) {
         m_dataSource->getPerfData(plot->property("plotid").toString(), plot->objectName(), start);
    }
}

void MainWindow::preferencesDlg()
{
    Preferences dlg;
    int rc=dlg.exec();
    if (rc == QDialog::Accepted){
        QSettings settings;
        settings.setValue(QStringLiteral("NAGIOS_URL"), dlg.nagiosURL());
    }
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    QSortFilterProxyModel *model=static_cast<QSortFilterProxyModel*>(ui->treeView->model());
    if (! model ){
        return;
    }
    model->setFilterWildcard(arg1);
    model->setFilterCaseSensitivity(Qt::CaseInsensitive);
    model->setFilterKeyColumn(-1);
}

void MainWindow::createPlot(QString plotid)
{



    QWidget *w1 = ui->splitter->widget(1);
    QVBoxLayout *vbl = static_cast<QVBoxLayout*>(w1->layout());

    if (! vbl){
        vbl = new QVBoxLayout;
        w1->setLayout(vbl);
    }

    ChartPlot *chart = new ChartPlot(plotid, this);
    vbl->addWidget(chart);
    chart->setObjectName(plotid);

    connect(static_cast<ChartPlot*>(chart), &ChartPlot::addDataSet, [=](const QString& plotid, const QString& dest){
            m_dataSource->getPerfData(plotid, dest);
    });

    m_dataSource->getPerfData(plotid, plotid);
}

void MainWindow::on_itemSelected(const QModelIndex &idx)
{
    QModelIndex i = idx;
    QStringList plotid;
    plotid.append(i.data().toString());
    // reconstruct the plotid
    while (i.parent().isValid()){
        i = i.parent();
        plotid.prepend(i.data().toString());
    }
    //create a new plot widget
    createPlot(plotid.join(PLOTID_SEP));
}


void MainWindow::on_toolButton_4h_clicked()
{
    QDateTime start(QDateTime::currentDateTime());
    start = start.addSecs(-4 * 3600);
    setTimeRange(start,QDateTime());
}

void MainWindow::on_toolButton_1d_clicked()
{
    QDateTime start(QDateTime::currentDateTime());
    start = start.addDays(-1);
    setTimeRange(start,QDateTime());
}

void MainWindow::on_toolButton_1w_clicked()
{
    QDateTime start(QDateTime::currentDateTime());
    start = start.addDays(-7);
    setTimeRange(start,QDateTime());
}

void MainWindow::on_toolButton_1m_clicked()
{
    QDateTime start(QDateTime::currentDateTime());
    start = start.addMonths(-1);
    setTimeRange(start,QDateTime());
}

void MainWindow::on_toolButton_1y_clicked()
{
    QDateTime start(QDateTime::currentDateTime());
    start = start.addYears(-1);
    setTimeRange(start,QDateTime());
}

void MainWindow::on_actionAbout_triggered()
{
    About().exec();
}
