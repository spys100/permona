#include "mainwindow.h"
#include <QApplication>

class permona : public QApplication
{
protected:
    virtual void commitDataRequest();
};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
