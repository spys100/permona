#include "preferences.h"
#include "ui_preferences.h"
#include <QSettings>

Preferences::Preferences(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Preferences)
{
    ui->setupUi(this);
    QSettings settings;
    ui->hostURL->setText(settings.value("NAGIOS_URL").toString());
}

Preferences::~Preferences()
{
    delete ui;
}

QString Preferences::nagiosURL() const
{
    return ui->hostURL->text();
}
