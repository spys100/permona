#ifndef HSMMODEL_H
#define HSMMODEL_H

#include <QAbstractItemModel>
#include <QSortFilterProxyModel>

typedef QStringList PlotIdList;

class HsmProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    HsmProxyModel(QObject *parent = 0 );
    /**
     * @brief appendToRoot
     * @param ids
     */
    void appendToRoot(const PlotIdList& ids);
protected:
    /**
     * @brief Allows filtering over all data
     * @param sourceRow
     * @param sourceParent
     * @return
     */
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;

};

class HsmModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit HsmModel(const PlotIdList &data, QObject *parent = 0);
    ~HsmModel();
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    /**
     * @brief calculate number of sub nodes for this item
     * @param parent
     * @return number of sub nodes
     */
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    /**
     * @brief appendToRoot
     * @param ids
     */
    void appendToRoot(const PlotIdList &ids);
private:
    class InternalTreeItem *m_internalTree;
signals:

public slots:

protected:
    QMimeData *mimeData(const QModelIndexList &indexes) const;
    QStringList mimeTypes() const;
    Qt::DropActions supportedDropActions() const;
};

#endif // HSMMODEL_H
