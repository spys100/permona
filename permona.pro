#-------------------------------------------------
#
# Project for permona a simple graph plotter for Nagios
#
#-------------------------------------------------

QT += core gui network charts concurrent

macx-clang:QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-inconsistent-missing-override

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = permona
TEMPLATE = app
CONFIG += c++11
ICON = permona.icns

# Rules for creating an installable package
include(package.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
    nagiosadapter.cpp \
    hsmmodel.cpp \
    authenticatordialog.cpp \
    preferences.cpp \
    simplecrypt.cpp \
    about.cpp \
    session.cpp \
    chartplot.cpp

HEADERS  += mainwindow.h \
    nagiosadapter.h \
    hsmmodel.h \
    authenticatordialog.h \
    preferences.h \
    simplecrypt.h \
    about.h \
    session.h \
    chartplot.h

FORMS    += mainwindow.ui \
    authenticatordialog.ui \
    preferences.ui \
    about.ui

RESOURCES += \
    resources.qrc
