#include "hsmmodel.h"
#include "qdebug.h"
#include <QMimeData>
#include <QDataStream>
#include <QtConcurrent>

#define ROOT_NODE quintptr(-1)

extern QString PLOTID_SEP;

extern InternalTreeItem invalidTreeItem;

/**
 * @brief The InternalTreeItem class
 */

class InternalTreeItem
{
public:
    InternalTreeItem(InternalTreeItem *parent, const QString& data):m_data(data),m_parent(parent) {}
    ~InternalTreeItem() {}
    InternalTreeItem& createChild(const QString& data) {
        InternalTreeItem child = InternalTreeItem(this, data);
        m_children.append(child);
        return m_children.last();
    }
    int childCount() const {
        return m_children.count();
    }
    InternalTreeItem& child(int childIndex) {
        if (childIndex < m_children.count()){
            return m_children[childIndex];
        }else{
            return invalidTreeItem;
        }
    }
    QString data() const {
        return m_data;
    }
    InternalTreeItem *parent() const {
        return m_parent;
    }
    int childPos(const InternalTreeItem& item) const{
        int pos = 0;
        for ( pos = 0; pos < m_children.count(); pos++) {
            if (&m_children.at(pos) == &item)
                break;
        }
        if (pos >= m_children.count())
            pos = -1; //not found
        return pos;
    }

    InternalTreeItem &lastChild() {
        if (m_children.isEmpty())
            return invalidTreeItem;
        else
            return m_children.last();
    }
private:
    QString m_data;
    InternalTreeItem *m_parent;
    QList<InternalTreeItem> m_children;
};

InternalTreeItem invalidTreeItem(Q_NULLPTR, Q_NULLPTR);

/**
 * @brief HsmModel::HsmModel
 * @param data must be sorted!
 * @param parent
 */

HsmModel::HsmModel(const PlotIdList &data, QObject *parent) :
    QAbstractItemModel(parent),
    m_internalTree(new InternalTreeItem(Q_NULLPTR, Q_NULLPTR))
{
    blockSignals(true);
    appendToRoot(data);
    blockSignals(false);
}

HsmModel::~HsmModel()
{
   delete m_internalTree;
}

int HsmModel::columnCount(const QModelIndex&) const
{
    return 1;
}

int HsmModel::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()){
        return m_internalTree->childCount();
    }
    return reinterpret_cast<InternalTreeItem*>(parent.internalPointer())->childCount();
}

QVariant HsmModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole)
        return QVariant();

    InternalTreeItem *dataPtr=reinterpret_cast<InternalTreeItem*>(index.internalPointer());
    Q_CHECK_PTR(dataPtr);
    return dataPtr->data();
}

Qt::ItemFlags HsmModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled;
}

QVariant HsmModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return section == 0 ? "Host" : "Service";

    return QVariant();
}

QModelIndex HsmModel::index(int row, int column, const QModelIndex &parent) const
{
    InternalTreeItem *parentItem;
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    if (!parent.isValid()) {
        parentItem = m_internalTree;
    }else{
        parentItem = reinterpret_cast<InternalTreeItem*>(parent.internalPointer());
    }
    if ( row >= rowCount(parent) )
        return QModelIndex();

    return createIndex(row, 0, &parentItem->child(row));
}

QModelIndex HsmModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    if ( index.internalPointer() == m_internalTree ) //root item
        return QModelIndex();
    InternalTreeItem *item = reinterpret_cast<InternalTreeItem*>(index.internalPointer());
    if ( item->parent() == m_internalTree ) //root item
        return QModelIndex();

    return createIndex(item->parent()->childPos(*item), 0, item->parent());
}

QStringList HsmModel::mimeTypes() const
{
    QStringList types;
    types << "application/vnd.plot.list";
    return types;
}

QMimeData *HsmModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData();
    QByteArray encodedData;

    QDataStream stream(&encodedData, QIODevice::WriteOnly);

    foreach (const QModelIndex &index, indexes) {
        if (index.isValid()) {
            QString id = data(index, Qt::DisplayRole).toString();
            QString parent = this->parent(index).data().toString();
            stream << parent + PLOTID_SEP + id;
        }
    }

    mimeData->setData("application/vnd.plot.list", encodedData);
    return mimeData;
}

void HsmModel::appendToRoot(const PlotIdList &ids)
{
    foreach(QString item, ids){
        QStringList parts = item.split(PLOTID_SEP);
        InternalTreeItem *parent = m_internalTree;
        QModelIndex parentIndex = QModelIndex();
        int row = 0;//= parent->childCount();

        for (int level = 0; level < parts.count(); ++level) {
            //qDebug() << "parentIndex: " << parentIndex;
            row = parent->childCount();
            if (parts[level] == parent->lastChild().data() ){
                parent = &parent->lastChild();
            }else{
                //qDebug() << "insert: " << parts[level] << " " << row;
                beginInsertRows(parentIndex, row, row);
                parent = &parent->createChild(parts[level]);
                endInsertRows();
            }

            parentIndex = index(qMax(row - 1, 0), 0, parentIndex);
        }
    }
}

Qt::DropActions HsmModel::supportedDropActions() const
{
    return Qt::CopyAction;
}

HsmProxyModel::HsmProxyModel(QObject *parent)
    :QSortFilterProxyModel(parent)
{

}

bool HsmProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex index0 = sourceModel()->index(sourceRow, 0, sourceParent);
    bool rv=sourceModel()->data(index0).toString().contains(filterRegExp());

    //also check all child nodes
    if (!rv){
        QModelIndex parentIndex = index0.isValid() ? index0 : index0.child(sourceRow, 0);

        if (parentIndex.isValid()){
            //get number of childs
            int n = sourceModel()->rowCount(parentIndex);
            while (--n >= 0){
                index0 = sourceModel()->index(n, 0, parentIndex);
                rv = sourceModel()->data(index0).toString().contains(filterRegExp());
                if (rv)
                    break;
            }
        }
    }
    return rv;

}

void HsmProxyModel::appendToRoot(const PlotIdList &ids)
{
    qobject_cast<HsmModel*>(sourceModel())->appendToRoot(ids);
}
