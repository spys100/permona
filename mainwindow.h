#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QDateTime>

namespace Ui {
class MainWindow;
}

class NagiosAdapter;
class SimpleCrypt;

typedef QStringList PlotIdList;
typedef QVector<double> DoubleVector;

class ChartPlot;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void createPlot(QString plotid);
    
public slots:
    void plotSingleGraph(ChartPlot *chart, const QString &title, const DoubleVector &x, const DoubleVector &y, const QColor &color);
    void updateServiceMap(const PlotIdList& hsmp);
    void plotGraph(const QString &plotid, const QString &dest, const DoubleVector &x, const DoubleVector &y );
    void authenticate(const QString &realm, QString *user, QString *passwd);
    void setTimeRange(const QDateTime start, const QDateTime end);
    void preferencesDlg();
private slots:
    void on_lineEdit_textChanged(const QString &arg1);
    void on_itemSelected(const QModelIndex &idx);
    void on_toolButton_4h_clicked();

    void on_toolButton_1d_clicked();

    void on_toolButton_1w_clicked();

    void on_toolButton_1m_clicked();

    void on_toolButton_1y_clicked();

    void on_actionAbout_triggered();

private:
    Ui::MainWindow *ui;
    NagiosAdapter *m_dataSource;
    QScopedPointer<SimpleCrypt> m_crypto;
};

#endif // MAINWINDOW_H
